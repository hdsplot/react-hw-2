import React, { Component } from 'react';
import './App.scss';
import Header from "./components/Header";
import Catalog from "./components/Catalog";
import Modal from "./components/Modal";
import Actions from "./components/Modal/components/Actions";
import Button from "./components/Button";
import { sendRequest } from "./helpers"

class App extends Component {
  state = {
    popupIsActive: false,
    goods: [],
    favorites: JSON.parse(localStorage.getItem('favorites')) || [],
    cart: JSON.parse(localStorage.getItem('cart')) || [],
    currentItem: {
      name: null,
      article: null
    }
  }

  componentDidMount = () => {
    sendRequest('goods.JSON')
      .then(data => {
        // console.log(data);
        this.setState({
          goods: data
        })
      })
  }

  render() {
    const { popupIsActive, cart, currentItem, favorites, goods } = this.state
    return (
      <div className="App">
        <Header logo={"logo.webp"} cartLength={cart.length} favLength={favorites.length}
        />
        <Catalog goods={goods}
          favorites={favorites}
          addToFav={this.toggleFav}
          showModal={this.renderCartModal} />
        {popupIsActive ? <Modal
          header={`Do you want to add ${currentItem.name} to cart?`}
          text={`You have ${cart.length} items in cart `}
          closeButton={true}
          close={this.toggleModal}
          modifier="cart"
          actions={<Actions buttons={[
            <Button key="0" text="Add" handler={() => this.addToCart(currentItem.article)} modifier="cart-modal" />,
            <Button key="1" text="Close" handler={this.toggleModal} modifier="cart-modal" />]
          } />}
        /> : void 0}
      </div>
    )
  }

  toggleFav = (article) => {
    const { favorites } = this.state;
    if (!favorites.includes(article)) {

      favorites.push(article);
    } else {
      favorites.splice(favorites.indexOf(article), 1)
    }
    this.setState({
      favorites: favorites
    })
    localStorage.setItem('favorites', JSON.stringify(favorites))
  }

  addToCart = (article) => {
    const { cart } = this.state;
    cart.push(article);
    this.setState({
      cart: cart
    })
    localStorage.setItem('cart', JSON.stringify(cart));
    this.toggleModal();
  }

  toggleModal = () => {
    this.setState({
      popupIsActive: !this.state.popupIsActive
    })
  }

  setCurrentItem(props) {
    this.setState({
      currentItem: {
        name: props.name,
        article: props.article
      }
    })
  }

  renderCartModal = (props) => {
    this.setCurrentItem(props);
    this.toggleModal();
  }

}

export default App;
