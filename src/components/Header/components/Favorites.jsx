import React from "react";
import { StarIcon } from "../../Icons";
import PropTypes from "prop-types";

class Favorites extends StarIcon {
  render() {
    const { favLength } = this.props;
    return (
      <>
        <div>
          <StarIcon />
          <span>{favLength}</span>
        </div>
      </>
    );
  }
}
export default Favorites;

Favorites.propTypes = {
  favLength: PropTypes.number,
};
