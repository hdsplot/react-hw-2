import React, { Component } from "react";
import "./Header.scss";
import PropTypes from "prop-types";
import Cart from "./components/Cart";
import Favorites from "./components/Favorites";

class Header extends Component {
  render() {
    const { logo, cartLength, favLength } = this.props;
    return (
      <header className="header">
        <div className="header__logo">
          <a href="">
            <img src={logo} alt="logotype" />
          </a>
        </div>
        <div className="header__icons">
          <Cart cartLength={cartLength} />
          <Favorites favLength={favLength} />
        </div>
      </header>
    );
  }
}
export default Header;

Header.propTypes = {
  logo: PropTypes.string,
  cartLength: PropTypes.number,
  favLength: PropTypes.number,
};

Header.defaultProps = {
  cartLength: null,
  favLength: null,
};
