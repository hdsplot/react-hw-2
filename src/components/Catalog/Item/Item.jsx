import React, { Component } from "react";
import "./Item.scss";
import Button from "../../Button";
import FavoriteStar from "./FavoriteStar";
import PropTypes from "prop-types";

class Item extends Component {
  render() {
    const {
      name,
      price,
      image,
      article,
      color,
      addToFav,
      showModal,
      favorites,
    } = this.props;
    const isFavorite = favorites.includes(article);

    return (
      <div className="product-item">
        <div className="product-item__image">
          <img src={image} alt={`product code: ${article}`} />
        </div>
        <h4 className="product-item__title">{name}</h4>
        <div className="product-item__description">
          <span className="product-item__code">{`article: ${article}`}</span>
          <span className="product-item__color">{`color: ${color}`}</span>

          <span className="product-item__price">
            {"price: "}
            <span className="product-item__price-sum">{price}</span>
          </span>
        </div>
        <FavoriteStar
          indicated={isFavorite}
          itemID={article}
          clickHandler={addToFav}
        />
        <Button
          modifier="cart"
          text="Add to cart"
          handler={() => showModal({ ...this.props })}
        />
      </div>
    );
  }
}

export default Item;

Item.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string,
  image: PropTypes.string,
  article: PropTypes.number,
  color: PropTypes.string,
  favorites: PropTypes.array,
  addToFav: PropTypes.func,
  showModal: PropTypes.func,
};
