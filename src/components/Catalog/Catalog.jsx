import React, { Component } from "react";
import Item from "./Item";
import "./Catalog.scss";
import PropTypes from "prop-types";

class Catalog extends Component {
  render() {
    const { goods, addToFav, showModal, favorites } = this.props;
    const items = goods.map((item) => {
      let { name, price, image, article, color } = item;
      return (
        <Item
          name={name}
          price={price}
          image={image}
          article={article}
          color={color}
          showModal={showModal}
          addToFav={addToFav}
          favorites={favorites}
          key={article}
        />
      );
    });

    return <div className="catalog">{items}</div>;
  }
}

export default Catalog;

Catalog.propTypes = {
  goods: PropTypes.array,
  favorites: PropTypes.array,
  addToFav: PropTypes.func,
  showModal: PropTypes.func,
};
