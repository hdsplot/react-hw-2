import React, { Component } from "react";
import CloseButton from "./components/CloseButton";
import "./Modal.scss";
import PropTypes, { string } from "prop-types";

class Modal extends Component {
  closeWindow = (event) => {
    event.preventDefault();

    if (
      event.target.className === "modal" ||
      event.target.hasAttribute("close")
    ) {
      this.props.close();
    }
  };
  render() {
    const { header, closeButton, text, actions, close, modifier } = this.props;

    return (
      <div className={"modal"} onClick={this.closeWindow}>
        <div className={`modal__content modal__content--${modifier}`}>
          <div className={`modal__header modal__header--${modifier}`}>
            <p className={`modal__header-text modal__header-text--${modifier}`}>
              {header}
            </p>
            {closeButton ? <CloseButton close={close} /> : void 0}
          </div>

          <div className={`modal__description modal__description--${modifier}`}>
            <p
              className={`modal__description-text modal__description-text--${modifier}`}
            >
              {text}
            </p>
          </div>

          {actions}
        </div>
      </div>
    );
  }
}

export default Modal;

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.object,
  close: PropTypes.func,
  modifier: string,
};

Modal.defaultProps = {
  modifier: "std",
  close: null,
};
