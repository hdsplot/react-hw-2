import React from "react";
import "./closeButton.scss";
import PropTypes from "prop-types";

const CloseButton = (props) => {
  return <div className={"close-btn"} onClick={props.close} />;
};

export default CloseButton;

CloseButton.propTypes = {
  close: PropTypes.func,
};

CloseButton.defaultProps = {
  close: null,
};
