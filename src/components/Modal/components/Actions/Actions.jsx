import React, { Component } from "react";
import "./Actions.scss";
import PropTypes from "prop-types";

class Actions extends Component {
  render() {
    const { buttons } = this.props;
    return <div className="actions">{buttons}</div>;
  }
}

export default Actions;

Actions.propTypes = {
  buttons: PropTypes.array,
};
